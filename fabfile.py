from fabric.api import run, task, local, cd, env
from fabric.contrib import files
from fabric.operations import put
from fabric.context_managers import shell_env

env.hosts = ['192.168.102.10']
env.user = 'deployer'

repo = 'deploy'
repo_path = '/home/deployer/repos/notifier.hub.legends.ismai.pt.git'
deploy_path = '/srv/sites/notifier.hub.legends.ismai.pt'

@task
def setup():
    mkdir(repo_path)
    mkdir(deploy_path)
    git_init()
    push()
    clone()

    with cd(deploy_path):
        if files.exists('{0}/{1}'.format(deploy_path, 'package.json')):
            run('npm install')


@task
def push():
    local('git push {0} master'.format(repo))


@task
def clone():
    if not files.exists('{0}/.git'.format(deploy_path)):
        run('git clone {repo} {deploy}'.format(
            repo=repo_path, deploy=deploy_path))


@task
def pull():
    with cd(deploy_path):
        run('git pull')

@task
def start():
    with cd(deploy_path):
        run('pm2 start notifier.js')

@task
def stop():
    with cd(deploy_path):
        run('pm2 stop notifier')

def mkdir(path):
    if not files.exists(path):
        run('mkdir -p {0}'.format(path))

def git_init():
    if not files.exists('{0}/config'.format(repo_path)):
        with cd(repo_path):
            run('git init --bare')


@task
def deploy():
    push()
    clone()
    pull()
    stop()
    start()
