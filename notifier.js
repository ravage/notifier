/* jshint strict:false */

var io        = require('socket.io')();
var redis     = require('redis').createClient();
var server    = require('http').createServer();
var listener  = null;

var address = '127.0.0.1';
var port    = 5000;

listener = function(channel, response) {
  this.emit(channel, JSON.parse(response));
};

redis.subscribe('notifications');
redis.subscribe('jobs');
redis.setMaxListeners(0);

io.on('connection', function(socket) {

  redis.on('message', listener.bind(socket));

  // socket.on('event', function(data) {});

  socket.on('disconnect', function() {
    redis.removeListener('message', listener);
  });

});

server.listen(port, address, function() {
  console.log('Listening on: %s:%d', address, port);
});

io.attach(server);
